Ova aplikacija omogućava korisniku praćenje svoje lokacije na Google Karti te mu se na ekran pomoću Geocoder klase ispisuje priblizna adresa. Na sucelju je prisutan i Button pritiskom na koji se pomocu result intenta pokrece kamera i slika fotografija. Nakon slikanja iskace notifikacija pritiskom na koju se uslikana slika prikazuje u galeriji. Ime slike zadaje se pomocu adrese iz geokodera. Pri prvom pokretanju aplikacije eksplicitno se trazi korisnikova dozvola za kroistenjem kamere, GPS-a i pisanje u vanjsku pohranu. 

Najveći problem bio je rad s kamerom, tj. stvaranje datoteke za pohranu fotografije i njeno otvaranje iz notifikacije. https://developer.android.com/training/camera/photobasics.html

Za koristenje mapa ukljucen je Google Play Services .